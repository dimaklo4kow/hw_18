<?php
/**
 * Created by PhpStorm.
 * User: Дом
 * Date: 29.12.2016
 * Time: 14:25
 */
try{
    $pdo = new PDO('mysql:host=localhost; dbname=news_blog', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET NAMES "utf8"');
}catch(PDOException $e){
    echo 'Не удалось подключится к базе данных<br>';
    echo $e->getMessage();
    exit();
}