<?php
/**
 * Created by PhpStorm.
 * User: Дом
 * Date: 29.12.2016
 * Time: 14:25
 */
include 'BDConnection.php';
require_once 'Classes/Publication.php';
require_once 'Classes/News.php';
require_once 'Classes/Article.php';
require_once 'Classes/PublicationsWriter.php';


$publication = new PublicationsWriter(News);
//$publication->getShortPreview(News);
//$publication->getShortPreview(Article);
//$news = News::create(1, $pdo);
//print_r($news->);
?>
<html>
<head>
    <title>Публикации</title>
    <meta charset="UTF-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container" style="width: 70%">
    <div class="row">
        <div class=".col-md-6">
            <H1 class="h1" style="text-align: center">Публикации</H1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h2 class="h2" style="text-align: center">Новости</h2>
            <?php $publication->getShortPreview(News); ?>
        </div>
        <div class="col-md-6">
            <h2 class="h2" style="text-align: center">Статьи</h2>
            <?php $publication->getShortPreview(Article); ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-footer" style=" flex: 0 0 auto">
            <p style="text-align: right">
                &copy Dmitry Klochkov
            </p>
        </div>
    </div>
</div>
</body>
</html>

