<?php

/**
 * Created by PhpStorm.
 * User: Дом
 * Date: 29.12.2016
 * Time: 14:51
 */
abstract class Publication
{
    public $id;
    public $title;
    public $shortText;
    public $fullText;

    /**
     * Publication constructor.
     * @param $id
     * @param $title
     * @param $shortText
     * @param $fullText
     */
    public function __construct($id, $title, $shortText, $fullText)
    {
        $this->id = $id;
        $this->title = $title;
        $this->shortText = $shortText;
        $this->fullText = $fullText;
    }

    public static function create($id, PDO $pdo)
    {
        $sql = $pdo->query("SELECT * FROM ".static::TABLE_NAME." WHERE id=$id");
        $res = $sql->fetch();
        $publication = new static($res['id'], $res['title'], $res['short_text'],
            $res['full_text'], $res[static::ATTRIBUTE]);
        return $publication;
    }


}