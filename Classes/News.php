<?php

/**
 * Created by PhpStorm.
 * User: Дом
 * Date: 29.12.2016
 * Time: 14:54
 */
class News extends Publication
{
    public $source;
    const TABLE_NAME = 'News';
    const ATTRIBUTE = 'source';

    public function __construct($id, $title, $shortText, $fullText, $source)
    {
        parent::__construct($id, $title, $shortText, $fullText);
        $this->source = $source;
    }
}