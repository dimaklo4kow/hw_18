<?php

/**
 * Created by PhpStorm.
 * User: Дом
 * Date: 29.12.2016
 * Time: 15:00
 */
class Article extends Publication
{
    public $author;
    const TABLE_NAME = 'Article';
    const ATTRIBUTE = 'author';

    public function __construct($id, $title, $shortText, $fullText, $author)
    {
        parent::__construct($id, $title, $shortText, $fullText);
        $this->author = $author;
    }
}