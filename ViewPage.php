<?php
include 'BDConnection.php';
require_once 'Classes/Publication.php';
require_once 'Classes/News.php';
require_once 'Classes/Article.php';
require_once 'Classes/PublicationsWriter.php';

$publication = new PublicationsWriter(News);
?>
<html>
<head>
    <title>Просмотр новости</title>
    <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container" style="width: 70%">
    <div class="row">
        <div class=".col-md-6" style="text-align: justify">
            <?php for ($i = 1; $i < 4; $i++): ?>
                <?php $news = News::create($i, $pdo);
                $article = Article::create($i, $pdo);
                ?>
                <?php if ($_GET['id1'] == $news->id): ?>
                    <h3 class="text-primary"><?php echo $news->title ?></h3>
                    <p><?php echo $news->fullText ?></p>
                    <p style="text-align: right">Источник: <?php echo $news->source ?></p>
                    <a class="btn btn-default" href="index.php">На главную</a><br><br>
                <?php endif; ?>

                <?php if ($_GET['id2'] == $article->id): ?>
                    <h3 class="text-primary"><?php echo $article->title ?></h3>
                    <p><?php echo $article->fullText ?></p>
                    <p style="text-align: right">Автор: <?php echo $article->author ?></p>
                    <a class="btn btn-default" href="index.php">На главную</a><br><br>
                <?php endif; ?>
            <?php endfor; ?>
        </div>

        <div class="panel panel-default">
            <div class="panel-footer">
                <p style="text-align: right">
                    &copy Dmitry Klochkov
                </p>
            </div>
        </div>
    </div>
</body>
</html>